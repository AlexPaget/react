import './App.css';
import Dialogs from './Component/Dialogs/Dialogs';
import Header from './Component/Header/Header';
import Navbar from './Component/Navbar/Navbar';
import Profile from './Component/Profile/Profile';
import {Route} from 'react-router-dom';

const App = (props) => {
	const { appState, dispatch, store } = props //деструкторизация

	return (
		<div className='app-wrapper'>
			<Header />
			<Navbar />
			<div className='content'>
				<Route 
					path='/profile' 
					render={() => (
						<Profile 
							post={appState.profilePage.postData} 
							dispatch={dispatch} 
							newPostText={appState.profilePage.newPostText} 
						/>
					)}
				/>
				<Route path='/message' render={ () => <Dialogs dialog={appState.messagePage.dialogData} message={appState.messagePage.messageData}
				 newMessageText={appState.messagePage.newMessageBody} store={store}/>} />		
			</div>
		</div>
	);
}

export default App;

// dialog={props.appState.messagePage.dialogData} message={props.appState.messagePage.messageData} фиговина передовалась в message