import { NavLink } from 'react-router-dom';
import classes from './Navbar.module.css'

const Navbar = () => {
  return (
    <div className={classes.nav}>
      <div className={classes.item}>
        <NavLink to='/profile' activeClassName={classes.active}>Profile</NavLink>
      </div>
      <div className={classes.item}>
      <NavLink to='/message' activeClassName={classes.active}>Message</NavLink>
      </div>
      <div className={classes.item}>
        <a href='*'>News</a>
      </div>
      <div className={classes.item}>
        <a href='*'>Music</a>
      </div>
      <div className={classes.item}>
        <a href='*'>Setting</a>
      </div>
    </div>
  )
};

export default Navbar;