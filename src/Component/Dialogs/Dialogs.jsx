import classes from './Dialogs.module.css';
import DialogItem from './DialogItem/DialogItem';
import DialogMessage from './DialogMessage/DialogMessage';
import { sendMessageCreator, updateNewMessageBodyCreator } from '../../redux/state';
import React, {createRef} from 'react'


const Dialogs = (props) => {

    const refMessageInput = createRef()

    let dialogElement = props.dialog.map((dia) => {
        return (<DialogItem name={dia.name} id={dia.id} />)
    });

    let messageElement = props.message.map((mes) => {
        return (<DialogMessage message={mes.message}/>)
    });

    //let newMessageBody = props.store.newMessageBody;


    let onSendMessageClick = () => {
        props.store.dispatch(sendMessageCreator())
    }

    let onNewMessageChange = () => {
        let text = refMessageInput.current.value;
        props.store.dispatch(updateNewMessageBodyCreator(text));
    }


    return (
        <div className={classes.dialogs}>
            <div className={classes.dialogsItem}>
                {dialogElement}
                {/* <DialogItem name={dialogData[0].name} id={dialogData[0].id}/>
                <DialogItem name={dialogData[1].name} id={dialogData[1].id}/> */}
            </div>
            <div className={classes.messages}>
               <div>{messageElement}</div>
               <div>
                   <div><textarea value={props.newMessageText} onChange={onNewMessageChange} ref={refMessageInput} placeholder='Enter your message'></textarea></div>
                   <div><button onClick={onSendMessageClick}>Send</button></div>
               </div>
                {/* <DialogMessage message={messageData[0].message} />
                <DialogMessage message={messageData[1].message} />
                <DialogMessage message={messageData[2].message} /> */}

            </div>
        </div>
    )
}

export default Dialogs;