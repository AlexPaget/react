import Post from './Post/Post';
import c from './MyPost.module.css';
import React from 'react';
import { UpdateNewPostTextActionCreator,addPostActionCreator} from '../../../redux/state';

const MyPost = (props) => {

  // let postData = [
  //   { message: 'Hello,my name Alex', like: '238' },
  //   { message: 'age 28', like: '32' },
  //   { message: 'vwskvkjd', like: '342' }
  // ]
    let newPostElement = React.createRef();

   
    let addPostElement = () => {
      //props.addPost ();
      props.dispatch(addPostActionCreator())
    }

    

  let MypostElement = props.post.map((post) => {
    return (<Post message={post.message} like={post.like} />)
  }
  );
  let onPostChange = () =>{
    let text = newPostElement.current.value;
     //props.updateNewPostText(text)
     let action = UpdateNewPostTextActionCreator(text);
     props.dispatch(action);
  }
  return (
     
    <div className={c.post}>
      <div>
        <h3> My post</h3>
        <div>
          <div><textarea onChange={onPostChange} ref={newPostElement} value={props.newPostText}/></div>
          <div><button onClick={addPostElement}>add post</button></div>
        </div>
      </div>
      {MypostElement}
      {/* <Post message={postData[0].message} like={postData[0].like} />
      <Post message={postData[1].message} like={postData[1].like} /> */}
    </div>
  )
};

export default MyPost;