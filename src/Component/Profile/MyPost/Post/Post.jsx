import classes from './Post.module.css';

const Post = (props) => {
  return (
    <div >
      <div className={classes.post}>
        <img src='https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png' alt='' />
        {props.message}
        <div>
          <span>Like {props.like}</span>
        </div>
      </div>
    </div>
  )
};

export default Post;