import MyPost from './MyPost/MyPost';
import classes from './Profile.module.css';
import ProfileInfo from './ProfileInfo/ProfileInfo';


const Profile = (props) => {



  return (
    <div className={classes.profile}>
          <ProfileInfo/>
         <MyPost post={props.post} dispatch={props.dispatch} newPostText={props.newPostText} />
    </div>
  )
};

export default Profile;