import classes from './ProfileInfo.module.css';

const ProfileInfo = () => {
  return (
    <div>
      <div className={classes.info}>
        <img src='https://klike.net/uploads/posts/2019-05/1556708032_1.jpg' alt='' />
      </div>
      <div className={classes.description}>
        ava  + description
      </div>
    </div>
  )
};

export default ProfileInfo;