// let postData = [
//     { message: 'Hello,my name Alex', like: '238' },
//     { message: 'age 28', like: '32' },
//     { message: 'vwskvkjd', like: '342' }
//   ]


//   let dialogData = [
//     { id: '1', name: 'Ilya' },
//     { id: '2', name: 'Evgeniy' },
//     { id: '3', name: 'Aleksandr' }
//   ]

//   let messageData = [
//   { id: '1', message: 'Hello' },
//   { id: '2', message: 'How are youo' },
//   { id: '3', message: 'It-kamasutra' }
//   ]

const ADD_POST = 'ADD-POST'
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT'
const UPDATE_NEW_MESSAGE_BODY = 'UPDATE_NEW_MESSAGE_BODY'
const SEND_MESSAGE = 'SEND_MESSAGE'


let store = {
  _state: {
    profilePage: {
      postData: [
        { message: 'Hello,my name Alex', like: '238' },
        { message: 'age 28', like: '32' },
        { message: 'vwskvkjd', like: '342' }
      ],
      newPostText: 'it-kamasutra'
    },

    messagePage: {
      dialogData: [
        { id: '1', name: 'Ilya' },
        { id: '2', name: 'Evgeniy' },
        { id: '3', name: 'Aleksandr' }
      ],
      messageData: [
        { id: '1', message: 'Hello' },
        { id: '2', message: 'How are youo' },
        { id: '3', message: 'It-kamasutra' }
      ],
      newMessageBody: '',
    },
   
  },
  _callSubscriber() {
    console.log('vsvbd')
  },

  getState() {
    return this._state;
  },
  subscribe(observer) {
    this._callSubscriber = observer
  },

  // addPost() {

  //   let newPost = {
  //     message: this._state.profilePage.newPostText,
  //     like: '56'
  //   }
  //   this._state.profilePage.postData.push(newPost)
  //   this._state.profilePage.newPostText = ''
  //   this._callSubscriber(this._state);
  // },
  // updateNewPostText(newText) {
  //   this._state.profilePage.newPostText = newText
  //   this._callSubscriber(this._state);
  // },

  

  dispatch(action) {
    if (action.type === ADD_POST) {
      let newPost = {
        message: this._state.profilePage.newPostText,
        like: '56'
      }
      this._state.profilePage.postData.push(newPost)
      this._state.profilePage.newPostText = ''
      this._callSubscriber(this._state);
    } else if (action.type === UPDATE_NEW_POST_TEXT){
      this._state.profilePage.newPostText = action.newText
      this._callSubscriber(this._state);
    } else if (action.type === UPDATE_NEW_MESSAGE_BODY){
      this._state.messagePage.newMessageBody = action.body;
      this._callSubscriber(this._state);}
      else if (action.type === SEND_MESSAGE){
      let body = this._state.messagePage.newMessageBody;
        console.log(this._state.messagePage)
        this._state.messagePage.messageData.push({ id: '4', message: body});
        this._state.messagePage.newMessageBody = '';
        this._callSubscriber(this._state);}
  }

}

export const addPostActionCreator = () => {
  return {
   type: ADD_POST
  }
};
export const UpdateNewPostTextActionCreator = (text) => {
  return {
    type: UPDATE_NEW_POST_TEXT ,newText: text
  }
};

export const sendMessageCreator = () => {
  return {
   type: SEND_MESSAGE
  }
};
export const updateNewMessageBodyCreator = (body) => {
  return {
    type: UPDATE_NEW_MESSAGE_BODY ,body: body
  }
};

export default store;

window.store = store;